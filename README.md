# WIN GitLab open science group - Education

This group is a central point to create and share educational tools which you have created.

## Adding your materials

### Add a copy of your existing project
If your material already exists in another project, you are invited to fork a complete copy and host it here. Materials shared here should generally be static, so please do not use this version of the project for development. Follow the guide below to fork your project to this group.

#### 1. Request access to the open-science gitlab group
In gitlab, click on `menu` (top left) then go to `groups` and `explore groups`. Click the group called `open-science`, then click `Request Access` on the group home page.

Once your access has been granted, you will be able to add material to any of the open-science subgroups (analysis, education, tasks, community).

#### 2. Fork your project to the appropriate open-science subgroup. 
Follow this [gitlab guide to create a fork of your project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork). When selecting the namespace for your fork, select the appropriate open-science subgroup.

## Issues or suggestions

If you spot any mistakes in this documentation or would like to improve the guidelines, please let us know!

### New to GitLab
The easiest way to comment on this documentation is by submitting an "issue". Please review any existing issues for this project and select "New Issue" if you've spotted anything new!

Review or submit a new issue here: [https://git.fmrib.ox.ac.uk/open-science/education/_readme/-/issues](https://git.fmrib.ox.ac.uk/open-science/education/_readme/-/issues)

### Familiar with GitLab
If you are expereinced with using gitlab, please submit changes to thius project documentation following the fork and merge process. 

More training and guideance on this process will be made available soon.

## Contact

This project is maintained by the Open WIN Community. Please contact cassandra.gouldvanpraag@psych.ox.ac.uk if you have any questions.

